//

//  Extensions.swift
//  Zazcar
//
//  Created by Lucas Algarra on 3/30/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit

// MARK: - Color

extension UIColor {
    class func randomColor () -> UIColor {
        return randomColorWithAlpha(1)
    }
    
    class func randomColorWithAlpha(alpha: CGFloat) -> UIColor {
        return UIColor(red:CGFloat(drand48()), green:CGFloat(drand48()) ,blue:CGFloat(drand48()) , alpha:alpha)
    }
    
    class func defaultDarkGrayColor () -> UIColor {
        return defaultDarkGrayColorWithAlpha(1)
    }
    
    class func defaultDarkGrayColorWithAlpha(alpha: CGFloat) -> UIColor {
        return UIColor(red:51/255.0, green:51/255.0 ,blue:51/255.0 , alpha:alpha)
    }
    
    class func defaultBlueColor () -> UIColor {
        return defaultBlueColorWithAlpha(1)
    }
    
    class func defaultBlueColorWithAlpha (alpha: CGFloat) -> UIColor {
        return UIColor(red:0/255.0, green:66/255.0 ,blue:149/255.0 , alpha:alpha)
    }
    
    class func defaultMustardColor () -> UIColor {
        return defaultMustardColorWithAlpha(1)
    }
    
    class func defaultMustardColorWithAlpha (alpha: CGFloat) -> UIColor {
        return UIColor(red:223/255.0, green:147/255.0 ,blue:4/255.0 , alpha:alpha)
    }
    
    // NavigationBar
    
    class func defaultNavigationBarColor() -> UIColor {
        return defaultNavigationBarColorWithAlpha(1)
    }
    
    class func defaultNavigationBarColorWithAlpha(alpha: CGFloat) -> UIColor {
        return defaultDarkGrayColorWithAlpha(alpha)
    }
}

// MARK: - Image

extension UIImage {
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        var imageSize = size
        if (imageSize.width <= 0) {
            imageSize.width = 1
        }
        
        if (imageSize.height <= 0) {
            imageSize.height = 1
        }
        
        let rect: CGRect = CGRectMake(0.0, 0.0, imageSize.width, imageSize.height)
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, 0)
        
        color.setFill()
        UIRectFill(rect)
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image
    }
    
    
}

// MARK: - Image

extension UIImageView {
    func maskImageViewWithColor(color: UIColor) {
        self.image = self.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        self.tintColor = color
    }
}

// MARK: - String

extension String {
    func localizableString () -> String {
        return NSLocalizedString(self, comment: "")
    }
}