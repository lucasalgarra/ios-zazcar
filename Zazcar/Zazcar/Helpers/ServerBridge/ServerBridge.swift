//
//  ServerBridge.swift
//  Zazcar
//
//  Created by Lucas Algarra on 3/28/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit
import Alamofire

typealias _JSON = [String: AnyObject]

let kServerAddress: String = "https://api.github.com"

class ServerBridge: NSObject {
    
}

func repositoriesWithLanguage(language: String, page: NSNumber, success: ((itemsArray: [[String: AnyObject]]?, totalRepositoriesCount: Int) -> Void)?, failure: ((NSError?) -> Void)?) {
    
    let url: String = "\(kServerAddress)/search/repositories"
    let parameters: [String: AnyObject] = ["q": "language:" + language, "sort": "stars", "page": page]
    
    
    Alamofire.request(.GET, url, parameters: parameters).responseJSON { response in
        if let valueDictionary: [String: AnyObject] = response.result.value as? [String : AnyObject] {
            print(response.result.value)
            if let itemsArray: [[String: AnyObject]] = valueDictionary["items"] as? [[String: AnyObject]] {
                if let totalRepositoriesCountNumber = valueDictionary["total_count"] as? NSNumber {
                    if let successBlock = success {
                        successBlock(itemsArray: itemsArray, totalRepositoriesCount: totalRepositoriesCountNumber.integerValue)
                    }
                }
            }
            else if let failureBlock = failure {
                failureBlock(nil)
            }
        }
        else if let failureBlock = failure {
            failureBlock(nil)
        }
    }
}

func pullRequestsWithUsername(username: String, repository: String, page: NSNumber, success: ((itemsArray: [[String: AnyObject]]?) -> Void)?, failure: ((NSError?) -> Void)?) {
    
    let url: String = "\(kServerAddress)/repos/\(username)/\(repository)/pulls"
    
    Alamofire.request(.GET, url, parameters: ["page": page]).responseJSON { response in
        print(response.response?.allHeaderFields)
        if let openArray: [[String: AnyObject]] = response.result.value as? [[String : AnyObject]] {
            if let successBlock = success {
                successBlock(itemsArray: openArray)
            }
        }
        else if let failureBlock = failure {
            failureBlock(nil)
        }
    }
}
