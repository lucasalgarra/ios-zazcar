//
//  RepositoriesViewController.swift
//  Zazcar
//
//  Created by Lucas Algarra on 3/28/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    let kLanguage = "Java"
    
    @IBOutlet var tableView: UITableView?
    @IBOutlet var tableFooterView: UIView?
    @IBOutlet var loadMoreActivityIndicatorView: UIActivityIndicatorView?
    @IBOutlet var firstLoadingActivityIndicatorView: UIActivityIndicatorView?
    
    var refreshControl: UIRefreshControl = UIRefreshControl()
    
    var repositoriesArray: [[String: AnyObject]] = []
    var totalRepositoriesCount: NSInteger = 0
    var page: Int = 1
    
    var didUpdate = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        configure()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !self.didUpdate {
            self.refreshControl.beginRefreshing()
            update()
            self.didUpdate = true
        }
    }
    
    // MARK: - Configure
    
    func configure () {
        configureNavigationBar()
        configureTableView()
        configureLoadMoreActivityIndicatorView()
        configureFirstLoadingActivityIndicatorView()
    }
    
    func configureNavigationBar () {
        self.title = String.init(format: "RepositoriesTitle".localizableString(), kLanguage);
        let image: UIImage = UIImage.imageWithColor(UIColor.defaultNavigationBarColor(), size: CGSizeZero)
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)
        self.navigationController?.navigationBar.barStyle = .Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    func configureTableView() {
        self.tableView?.tableFooterView = nil
        self.refreshControl.tintColor = UIColor.defaultDarkGrayColor()
        self.refreshControl.addTarget(self, action: #selector(RepositoriesViewController.update), forControlEvents: .ValueChanged)
        self.tableView?.hidden = true
    
        self.tableView?.addSubview(self.refreshControl)
        self.refreshControl.superview?.sendSubviewToBack(self.refreshControl)
    }
    
    func configureLoadMoreActivityIndicatorView() {
        self.loadMoreActivityIndicatorView?.color = UIColor.defaultDarkGrayColor()
    }
    
    func configureFirstLoadingActivityIndicatorView() {
        self.firstLoadingActivityIndicatorView?.color = UIColor.defaultDarkGrayColor()
        self.firstLoadingActivityIndicatorView?.startAnimating()
        self.firstLoadingActivityIndicatorView?.hidesWhenStopped = true;
    }
    
    // MARK: - Update
    
    func update() {
        self.page = 1
        let pageNumber = NSNumber.init(integer: self.page)
        repositoriesWithLanguage(kLanguage, page: pageNumber,
                                 success: { (object, totalRepositoriesCount) in
                                    self.refreshControl.endRefreshing()
                                    self.firstLoadingActivityIndicatorView?.stopAnimating()
                                    self.tableView?.hidden = false
                                    
                                    if let repositoriesArray = object {
                                        self.repositoriesArray = repositoriesArray
                                        self.page += 1
                                    }
                                    else {
                                        self.repositoriesArray = []
                                    }
                                    
                                    self.totalRepositoriesCount = totalRepositoriesCount
                                    
                                    if self.repositoriesArray.count == self.totalRepositoriesCount {
                                        self.tableView?.tableFooterView = nil
                                    }
                                    else {
                                        self.tableView?.tableFooterView = self.tableFooterView
                                    }
                                    
                                    self.tableView!.reloadData()
            },
                                 failure: { (error) in
                                    self.refreshControl.endRefreshing()
                                    self.firstLoadingActivityIndicatorView?.stopAnimating()
                                    self.tableView?.hidden = false
            }
        )
    }
    
    func loadMore() {
        let pageNumber = NSNumber.init(integer: self.page)
        repositoriesWithLanguage(kLanguage, page: pageNumber,
                                 success: { (object, totalRepositoriesCount) in
                                    if let repositoriesArray = object {
                                        self.repositoriesArray.appendContentsOf(repositoriesArray)
                                        self.page += 1
                                    }
                                    
                                    self.totalRepositoriesCount = totalRepositoriesCount
                                    
                                    if self.repositoriesArray.count == self.totalRepositoriesCount {
                                        self.tableView?.tableFooterView = nil
                                    }
                                    else {
                                        self.tableView?.tableFooterView = self.tableFooterView
                                    }
                                    
                                    self.tableView!.reloadData()
            },
                                 failure: { (error) in
                                    
            }
        )
    }
    
    // MARK: - UITableViewDataSource, UITableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositoriesArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let repositoryDictionary: [String: AnyObject] = self.repositoriesArray[indexPath.row]
        
        let cell: RepositoryTableViewCell = (tableView.dequeueReusableCellWithIdentifier("RepositoryCell") as? RepositoryTableViewCell)!
        cell.setRepository(repositoryDictionary)
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == self.repositoriesArray.count - 1 &&  self.repositoriesArray.count != self.totalRepositoriesCount && self.page > 1 {
            loadMore()
        }
    }
    
    // MARK: - Storyboard
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showPullRequests") {
            let pullRequestsViewController = segue.destinationViewController as! PullRequestsViewController
            
            let row: Int = (self.tableView?.indexPathForSelectedRow)!.row
            
            let repositoryDictionary: [String: AnyObject] = self.repositoriesArray[row]
            pullRequestsViewController.repositoryDictionary = repositoryDictionary
            
            self.tableView?.deselectRowAtIndexPath((self.tableView?.indexPathForSelectedRow)!, animated: true)
        }
    }
}
