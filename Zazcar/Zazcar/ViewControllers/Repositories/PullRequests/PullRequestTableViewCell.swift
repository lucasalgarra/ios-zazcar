//
//  PullRequestTableViewCell.swift
//  Zazcar
//
//  Created by Lucas Algarra on 3/31/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {
    
    let kPullRequestTitleKey = "title"
    let kPullRequestBodyKey = "body"
    let kPullRequestUserKey = "user"
    let kPullRequestUserUsernameKey = "login"
    let kPullRequestUserAvatarKey = "avatar_url"
    
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var descriptionLabel: UILabel?
    @IBOutlet var usernameLabel: UILabel?
    @IBOutlet var userPhotoImageView: UIImageView?
    
    let userImage: UIImage = UIImage.init(named: "UserPhoto")!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configure()
    }
    
    // MARK: - Configure
    
    func configure() {
        configureTitleLabel()
        configureDescriptionLabel()
        configureUsernameLabel()
        configureUserPhotoImageView()
    }
    
    func configureTitleLabel() {
        self.titleLabel?.font = UIFont.systemFontOfSize(17)
        self.titleLabel?.textColor = UIColor.defaultBlueColor()
        self.titleLabel?.backgroundColor = UIColor.clearColor()
    }
    
    func configureDescriptionLabel() {
        self.descriptionLabel?.font = UIFont.systemFontOfSize(14)
        self.descriptionLabel?.textColor = UIColor.defaultDarkGrayColor()
        self.descriptionLabel?.numberOfLines = 2
        self.descriptionLabel?.backgroundColor = UIColor.clearColor()
    }
    
    func configureUsernameLabel() {
        self.usernameLabel?.font = UIFont.boldSystemFontOfSize(12)
        self.usernameLabel?.textColor = UIColor.defaultBlueColor()
        self.usernameLabel?.backgroundColor = UIColor.clearColor()
        self.usernameLabel?.minimumScaleFactor = 0.83
    }
    
    func configureUserPhotoImageView() {
        self.userPhotoImageView?.layer.cornerRadius = (userPhotoImageView?.frame.size.height)! / 2.0
        self.userPhotoImageView?.layer.masksToBounds = true
        self.userPhotoImageView?.layer.borderColor = UIColor.clearColor().CGColor
        self.userPhotoImageView?.layer.borderWidth = 0
        self.userPhotoImageView?.backgroundColor = UIColor.clearColor()
    }
    
    // MARK: - SetRepository
    
    func setPullRequest(pullRequestDictionary: [String: AnyObject]) {
        self.titleLabel?.text = nil
        self.descriptionLabel?.text = nil
        self.usernameLabel?.text = nil
        
        let title: String? = pullRequestDictionary[kPullRequestTitleKey] as? String
        self.titleLabel?.text = title
        
        let description: String? = pullRequestDictionary[kPullRequestBodyKey] as? String
        self.descriptionLabel?.text = description
        
        let numberFormatter = NSNumberFormatter()
        numberFormatter.numberStyle = .DecimalStyle
        
        
        if let userDictionary = pullRequestDictionary[kPullRequestUserKey] as? [String: AnyObject] {
            let username: String? = userDictionary[kPullRequestUserUsernameKey] as? String
            self.usernameLabel?.text = username
            
            self.userPhotoImageView?.image = self.userImage
            if let urlString = userDictionary[kPullRequestUserAvatarKey] as? String {
                if let url: NSURL = NSURL.init(string: urlString) {
                    self.userPhotoImageView?.sd_setImageWithURL(url, placeholderImage: self.userImage, completed: { (image, error, imageCacheType, url) in
                        if image != nil {
                            self.userPhotoImageView?.image = image
                        }
                        else {
                            self.userPhotoImageView?.image = self.userImage
                        }
                    });
                }
            }
        }
        
        
    }
    
    // MARK: - Resize
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        resize()
    }
    
    func resize() {
        resizeDescriptionLabel()
    }
    
    func resizeDescriptionLabel() {
        var descriptionLabelFrame: CGRect = (self.descriptionLabel?.frame)!
        self.descriptionLabel?.sizeToFit()
        descriptionLabelFrame.size.height = (self.descriptionLabel?.frame.size.height)!
        self.descriptionLabel?.frame = descriptionLabelFrame
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.setHighlighted(selected, animated: animated)
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        self.userPhotoImageView?.backgroundColor = UIColor.clearColor()
    }

}
