//
//  PullRequestsViewController.swift
//  Zazcar
//
//  Created by Lucas Algarra on 3/28/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit

class PullRequestsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var repositoryDictionary: [String: AnyObject] = Dictionary()
    
    let kPageCount = 30
    
    let kRepositoryNameKey = "name"
    let kRepositoryUrlKey = "html_url"
    let kRepositoryOwnerKey = "owner"
    let kRepositoryOwnerUsenameKey = "login"
    
    let kPullRequestStateKey = "state"
    
    let kStateOpen = "open"
    
    @IBOutlet var tableView: UITableView?
    @IBOutlet var tableFooterView: UIView?
    @IBOutlet var loadMoreActivityIndicatorView: UIActivityIndicatorView?
    @IBOutlet var firstLoadingActivityIndicatorView: UIActivityIndicatorView?
    @IBOutlet var openCountView: UIView?
    @IBOutlet var openCountLabel: UILabel?
    var refreshControl: UIRefreshControl = UIRefreshControl()
    
    var pullRequestsArray: [[String: AnyObject]] = []
    
    var didUpdate = false
    
    var page: Int = 1

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        configure()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !self.didUpdate {
            self.refreshControl.beginRefreshing()
            update()
            self.didUpdate = true
        }
    }
    
    // MARK: - Configure
    
    func configure () {
        self.view.backgroundColor = UIColor.whiteColor();
        
        configureNavigationBar()
        configureTableView()
        configureLoadMoreActivityIndicatorView()
        configureFirstLoadingActivityIndicatorView()
        configureOpenCountView()
    }
    
    func configureNavigationBar () {
        self.title = "PullRequestsTitle".localizableString();
        let image: UIImage = UIImage.imageWithColor(UIColor.defaultNavigationBarColor(), size: CGSizeZero)
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)
        self.navigationController?.navigationBar.barStyle = .Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    
    func configureTableView() {
        self.tableView?.tableFooterView = nil
        
        self.tableView?.backgroundColor = self.view.backgroundColor;
        self.tableView?.hidden = true
        
        self.refreshControl.tintColor = UIColor.defaultDarkGrayColor()
        self.refreshControl.addTarget(self, action: #selector(RepositoriesViewController.update), forControlEvents: .ValueChanged)
        
        self.tableView?.addSubview(self.refreshControl)
        self.refreshControl.superview?.sendSubviewToBack(self.refreshControl)
    }
    
    func configureLoadMoreActivityIndicatorView() {
        self.loadMoreActivityIndicatorView?.color = UIColor.defaultDarkGrayColor()
    }
    
    func configureFirstLoadingActivityIndicatorView() {
        self.firstLoadingActivityIndicatorView?.color = UIColor.defaultDarkGrayColor()
        self.firstLoadingActivityIndicatorView?.startAnimating()
        self.firstLoadingActivityIndicatorView?.hidesWhenStopped = true;
    }
    
    func configureOpenCountView() {
        self.openCountView?.backgroundColor = UIColor.whiteColor()
        self.openCountLabel?.font = UIFont.boldSystemFontOfSize(14)
        self.openCountLabel?.textColor = UIColor .defaultDarkGrayColor()
        
        if var openCountViewFrame = self.openCountView?.frame {
            openCountViewFrame.origin.y = -openCountViewFrame.size.height
            self.openCountView?.frame = openCountViewFrame
        }
        
        var lineViewFrame: CGRect = CGRectZero
        lineViewFrame.size.height = 1
        lineViewFrame.size.width = (self.openCountView?.frame.size.width)!
        lineViewFrame.origin.y = (self.openCountView?.frame.size.height)! - lineViewFrame.size.height
        let lineView: UIView = UIView(frame: lineViewFrame)
        lineView.autoresizingMask = [.FlexibleWidth, .FlexibleTopMargin]
        lineView.backgroundColor = UIColor.defaultDarkGrayColor()
        self.openCountView?.addSubview(lineView)
    }
    
    // MARK: - Load
    
    func loadOpenCountView() {
        var openCount = 0
        for var pullRequestDictionary:[String: AnyObject] in self.pullRequestsArray {
            if let status: String = pullRequestDictionary[kPullRequestStateKey] as? String {
                if status == kStateOpen {
                    openCount += 1
                }
            }
        }
        
        let closeCount = self.pullRequestsArray.count - openCount
        
        let openText: String = "\(openCount) opened"
        let closeText: String = "\(closeCount) closed"
        let openCountText = "\(openText) / \(closeText)"
        
        let openCountTextMutableAttributedString:NSMutableAttributedString = NSMutableAttributedString(string: openCountText)
        let range = (openCountText as NSString).rangeOfString(openText)
        openCountTextMutableAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.defaultMustardColor(), range: range)
        self.openCountLabel?.attributedText = openCountTextMutableAttributedString
        
        if var openCountViewFrame = self.openCountView?.frame {
            openCountViewFrame.origin.y = 0
            
            UIView.animateWithDuration(0.25, delay: 0, options: .CurveEaseOut, animations: {
                self.openCountView?.frame = openCountViewFrame
                }, completion: { finished in
                    
            })
        }
        
    }
    
    // MARK: - Update
    
    func update() {
        self.page = 1
        
        loadMore()
    }
    
    func loadMore() {
        if let repositoryName = self.repositoryDictionary[kRepositoryNameKey] as? String {
            if let ownerDictionary = repositoryDictionary[kRepositoryOwnerKey] as? [String: AnyObject] {
                if let username = ownerDictionary[kRepositoryOwnerUsenameKey] as? String {
                    pullRequestsWithUsername(username, repository: repositoryName, page: self.page, success: { (itemsArray) in
                        
                        self.refreshControl.endRefreshing()
                        self.firstLoadingActivityIndicatorView?.stopAnimating()
                        self.tableView?.hidden = false
                        
                        if let pullRequestsArray = itemsArray {
                            if self.page == 1 {
                                self.pullRequestsArray = pullRequestsArray
                            }
                            else {
                                self.pullRequestsArray.appendContentsOf(pullRequestsArray)
                            }
                            
                            self.page += 1
                            
                            if pullRequestsArray.count == self.kPageCount {
                                self.tableView?.tableFooterView = self.tableFooterView
                            }
                            else {
                                self.tableView?.tableFooterView = nil
                            }
                        }
                        else {
                            self.pullRequestsArray = []
                            self.tableView?.tableFooterView = nil
                        }
                        
                        
                        
                        self.tableView!.reloadData()
                        self.loadOpenCountView()
                        
                        }, failure: { (error) in
                            self.refreshControl.endRefreshing()
                            self.firstLoadingActivityIndicatorView?.stopAnimating()
                            self.tableView?.hidden = false
                    })
                }
            }
        }
    }
    
    // MARK: - UITableViewDataSource, UITableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pullRequestsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let pullRequestDictionary: [String: AnyObject] = self.pullRequestsArray[indexPath.row]
        
        let cell: PullRequestTableViewCell = (tableView.dequeueReusableCellWithIdentifier("PullRequestCell") as? PullRequestTableViewCell)!
        cell.setPullRequest(pullRequestDictionary)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pullRequestDictionary: [String: AnyObject] = self.pullRequestsArray[indexPath.row]
        
        if let urlString = pullRequestDictionary[kRepositoryUrlKey] as? String {
            if let URL = NSURL(string: urlString) {
                UIApplication.sharedApplication().openURL(URL)
            }
        }
        
        if let selectedIndexPath = self.tableView?.indexPathForSelectedRow {
            self.tableView?.deselectRowAtIndexPath(selectedIndexPath, animated: true)
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == self.pullRequestsArray.count - 1 && self.page > 1 {
            loadMore()
        }
    }
}
