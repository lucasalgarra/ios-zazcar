//
//  RepositoryTableViewCell.swift
//  Zazcar
//
//  Created by Lucas Algarra on 3/30/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {
    let kRepositoryNameKey = "name"
    let kRepositoryDescriptionKey = "description"
    let kRepositoryStarsKey = "stargazers_count"
    let kRepositoryForksKey = "forks"
    let kRepositoryOwnerKey = "owner"
    let kRepositoryOwnerUsenameKey = "login"
    let kRepositoryOwnerAvatarKey = "avatar_url"
    
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var descriptionLabel: UILabel?
    @IBOutlet var usernameLabel: UILabel?
    @IBOutlet var userPhotoImageView: UIImageView?
    @IBOutlet var forkImageView: UIImageView?
    @IBOutlet var forksCountLabel: UILabel?
    @IBOutlet var starImageView: UIImageView?
    @IBOutlet var starsCountLabel: UILabel?
    
    let userImage: UIImage = UIImage.init(named: "UserPhoto")!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        configure()
    }
    
    // MARK: - Configure
    
    func configure() {
        configureNameLabel()
        configureDescriptionLabel()
        configureUsernameLabel()
        configureUserPhotoImageView()
        configureForkImageView()
        configureForksCountLabel()
        configureStarImageView()
        configureStarsCountLabel()
    }
    
    func configureNameLabel() {
        self.nameLabel?.font = UIFont.systemFontOfSize(17)
        self.nameLabel?.textColor = UIColor.defaultBlueColor()
        self.nameLabel?.backgroundColor = UIColor.clearColor()
    }
    
    func configureDescriptionLabel() {
        self.descriptionLabel?.font = UIFont.systemFontOfSize(14)
        self.descriptionLabel?.textColor = UIColor.defaultDarkGrayColor()
        self.descriptionLabel?.numberOfLines = 2
        self.descriptionLabel?.backgroundColor = UIColor.clearColor()
    }
    
    func configureUsernameLabel() {
        self.usernameLabel?.font = UIFont.boldSystemFontOfSize(12)
        self.usernameLabel?.textColor = UIColor.defaultBlueColor()
        self.usernameLabel?.backgroundColor = UIColor.clearColor()
        self.usernameLabel?.minimumScaleFactor = 0.83
    }
    
    func configureUserPhotoImageView() {
        self.userPhotoImageView?.layer.cornerRadius = (userPhotoImageView?.frame.size.height)! / 2.0
        self.userPhotoImageView?.layer.masksToBounds = true
        self.userPhotoImageView?.layer.borderColor = UIColor.clearColor().CGColor
        self.userPhotoImageView?.layer.borderWidth = 0
        self.userPhotoImageView?.backgroundColor = UIColor.clearColor()
    }
    
    func configureForkImageView() {
        forkImageView?.maskImageViewWithColor(UIColor.defaultMustardColor())
    }
    
    func configureForksCountLabel() {
        self.forksCountLabel?.font = UIFont.systemFontOfSize(17)
        self.forksCountLabel?.textColor = UIColor.defaultMustardColor()
    }
    
    func configureStarImageView() {
        starImageView?.maskImageViewWithColor(UIColor.defaultMustardColor())
    }
    
    func configureStarsCountLabel() {
        self.starsCountLabel?.font = self.forksCountLabel?.font
        self.starsCountLabel?.textColor = self.forksCountLabel?.textColor
    }
    
    // MARK: - SetRepository
    
    func setRepository(repository: [String: AnyObject]) {
        self.nameLabel?.text = nil
        self.descriptionLabel?.text = nil
        self.forksCountLabel?.text = nil
        self.usernameLabel?.text = nil
        
        let name: String? = repository[kRepositoryNameKey] as? String
        self.nameLabel?.text = name
        
        let description: String? = repository[kRepositoryDescriptionKey] as? String
        self.descriptionLabel?.text = description
        
        let numberFormatter = NSNumberFormatter()
        numberFormatter.numberStyle = .DecimalStyle
        
        let forkCount: NSNumber? = repository[kRepositoryForksKey] as? NSNumber
        self.forksCountLabel?.text = numberFormatter.stringFromNumber(forkCount!)
        
        let starCount: NSNumber? = repository[kRepositoryStarsKey] as? NSNumber
        self.starsCountLabel?.text = numberFormatter.stringFromNumber(starCount!)
        
        if let ownerDictionary = repository[kRepositoryOwnerKey] as? [String: AnyObject] {
            let username: String? = ownerDictionary[kRepositoryOwnerUsenameKey] as? String
            self.usernameLabel?.text = username
            
            self.userPhotoImageView?.image = self.userImage
            if let urlString = ownerDictionary[kRepositoryOwnerAvatarKey] as? String {
                if let url: NSURL = NSURL.init(string: urlString) {
                    self.userPhotoImageView?.sd_setImageWithURL(url, placeholderImage: self.userImage, completed: { (image, error, imageCacheType, url) in
                        if image != nil {
                            self.userPhotoImageView?.image = image
                        }
                        else {
                            self.userPhotoImageView?.image = self.userImage
                        }
                    });
                }
            }
        }
        
        
    }
    
    // MARK: - Resize
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        resize()
    }
    
    func resize() {
        resizeDescriptionLabel()
    }
    
    func resizeDescriptionLabel() {
        var descriptionLabelFrame: CGRect = (self.descriptionLabel?.frame)!
        self.descriptionLabel?.sizeToFit()
        descriptionLabelFrame.size.height = (self.descriptionLabel?.frame.size.height)!
        self.descriptionLabel?.frame = descriptionLabelFrame
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        self.setHighlighted(selected, animated: animated)
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        self.userPhotoImageView?.backgroundColor = UIColor.clearColor()
    }
}
